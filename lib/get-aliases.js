const Proc = require('child_process');

module.exports = function (done) {
  Proc.exec('alias', function (err, stdout, stderr) {

    /* $lab:coverage:off$ */
    if (err) return done(err);
    /* $lab:coverage:on$ */

    done(null, stdout);
  });
};
