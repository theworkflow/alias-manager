const Code = require('code');
const Lab = require('lab');
const Sinon = require('sinon');


const GetAliases = require('../../lib/get-aliases');

var callback = function (error, result) {
  return arguments;
};

var lab = exports.lab = Lab.script();

var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;
var before = lab.before;
var beforeEach = lab.beforeEach;
var after = lab.after;
var afterEach = lab.afterEach;

describe('get-aliases', function() {
  var command;

  beforeEach(function (done) {
    command = 'alias';
    done();
  });

  afterEach(function (done) {
    command = undefined;
    done();
  });

  it('exports a function', function (done) {
    expect(GetAliases).to.be.an.instanceOf(Function);
    done();
  });

  it('returns a callback function', function (done) {
    GetAliases(callback);
    expect(callback).to.be.a.function();
    done();
  });

  describe('passed results', function () {

    var stub;

    beforeEach(function (done) {
      stub = GetAliases = Sinon.stub().yields(null, "foo=bar");
      done();
    });

    afterEach(function (done) {
      stub.reset();
      done();
    });

    it('passes results from child_process.exec', function (done) {
      stub(function (err, results) {
        expect(err).to.not.exist();
        expect(results).to.be.a.string();
        expect(results).to.equal("foo=bar");
        done();
      });
    });
  });

  describe('passed errors from child_process.exec', function () {

    var stub;

    beforeEach(function (done) {
      stub = GetAliases = Sinon.stub().yields(new Error('error'));
      done();
    });

    afterEach(function (done) {
      stub.reset();
      done();
    });

    it('passes errors', function (done) {
      stub(function (err, results) {
        expect(err).to.be.instanceOf(Error);
        expect(err.message).to.equal('error');
        expect(results).to.not.exist();
        done();
      });
    });
  });
});
